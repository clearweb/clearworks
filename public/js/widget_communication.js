listener_widgets = new Array();
uids = new Array();

clear_listener_callbacks = {};
clear_params = {};

function queueRequest(request, type, key) {
    if (typeof(window['request_queue_'+type+'_'+key]) == 'undefined') {
	window['request_queue_'+type+'_'+key] = new Array();
    }
    
    queue = window['request_queue_'+type+'_'+key];
    if(queue[queue.length - 1]) {
        queue[queue.length - 1].abort();
    }
    
    queue[queue.length] = request;
}

function initPageState(params) {
    if ($.isEmptyObject(clear_params)) {
	clear_params = params;
    }
}

function setMultiPageState(params, ignore_widget_name) {
    $.each(params, function (param_name, param_value) {
	setPageState(param_name, param_value);
    });
}

function setPageState(attr, val, ignore_widget_name) {
    $(document).ready(function() {
	oldVal = '';
	
	if (typeof(clear_params[attr]) != 'undefined') {
	    oldVal = clear_params[attr];
	}
	
	if (typeof(clear_listener_callbacks[attr]) != 'undefined') {

	    
	    clear_params[attr] = val;
	    
	    for(i=0; i < clear_listener_callbacks[attr].length; i++) {
		callback = clear_listener_callbacks[attr][i];
		callback('', oldVal, val);
	    }
	}
	
	if (
	    oldVal != val
		&& typeof(listener_widgets[attr]) !== 'undefined'
	) {
	    $(listener_widgets[attr]).each(function(index, widget_name) {
		if (
		    typeof(ignore_widget_name) == 'undefined'
			|| ignore_widget_name != widget_name
		) {
		    updateWidget(widget_name);
		}
	    });
	}
    });
}

function getPageState(attr) {
    return clear_params[attr];
}

function isSubscribedUID(uid) {
    if(uids.indexOf(uid) > 0) {
	return true;
    } else {
	uids.push(uid);
	return false;
    }
}

function subscribeToPageState(attr, callback, unique_id) {
    if (typeof(unique_id) == 'undefined' || ! isSubscribedUID(unique_id)) {
	if (typeof(clear_listener_callbacks[attr]) == 'undefined') {
	    clear_listener_callbacks[attr] = new Array();
	}
	
	clear_listener_callbacks[attr].push(callback);
    }
}

/*
 * updates the widget content through ajax.
 */
function updateWidget(widget_name) {
    var params = clear_params;
    var url_postfix = '';
    
    var widget = $('.communicating-widget[data-widget-name="' + widget_name + '"]');
    
    var update_url = widget.attr('data-update-url');
    
    for(var key in params) {
	value = params[key];
	url_postfix += '/' + key + '/' + value;
    }
    
    if (update_url.substring(update_url.length - 1) != '/' && url_postfix.substring(0, 1) != '/') {
	url = update_url + '/' + url_postfix;
    } else if(update_url.substring(update_url.length - 1) == '/' && url_postfix.substring(0, 1) == '/') {
	url = update_url.substring(0, update_url.length - 1) + url_postfix;
    } else {
	url = update_url + url_postfix;
    }
    
    
    xhr = $.get(url, {}, function(data) {
	widget.replaceWith(data);
    }).fail(function() {
	//alert('there was an error');
	console.log('ajax request stopped for: '+url);
    });
    
    queueRequest(xhr, 'widget_update', widget_name);
}

function updateCurrentWidget(element) {
    communicating_widget = element.closest('.widget').find('.communicating-widget');
    if (typeof(communicating_widget) == "undefined" || communicating_widget.length == 0) {
	console.log('trying to update current widget from outside a widget');
    } else {
	params = clear_params;
	
	updateWidget(communicating_widget.attr('data-widget-name'));
    }
}