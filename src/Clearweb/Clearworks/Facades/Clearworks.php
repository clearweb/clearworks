<?php namespace Clearweb\Clearworks\Facades;

/**
 * This file is to communicate with Laravel
 */

use Illuminate\Support\Facades\Facade;

class Clearworks extends Facade {
	
	/**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'clearworks'; }
}