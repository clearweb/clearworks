<?php namespace Clearweb\Clearworks\Asset;

interface IAssetBag
{
    public function addScript($name, $url, array $dependencies=array());
	public function addStyle($style);
    public function getStyles();
    public function setStyles($styles);
    public function getScripts();
    public function setScripts($scripts);
    function mergeWith(IAssetBag $bag2);
}