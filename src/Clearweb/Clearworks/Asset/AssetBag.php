<?php namespace Clearweb\Clearworks\Asset;

class AssetBag implements IAssetBag
{
    private $scripts = array();
	private $styles  = array();

    public function addScript($name, $url, array $dependencies=array())
    {
        $this->scripts[$name] = array('name'=>$name, 'url'=>$url, 'dependencies'=>$dependencies);
		return $this;
	}
	
	public function addStyle($style)
    {
		$this->styles[] = $style;
		return $this;
	}
    
    public function getStyles()
    {
        return $this->styles;
    }
    
    public function setStyles($styles)
    {
        $this->styles = $styles;
        return $this;
    }
    
    public function getScripts()
    {
        return $this->scripts;
    }

    public function setScripts($scripts)
    {
        $this->scripts = $scripts;
        return $this;
    }

    
    function mergeWith(IAssetBag $bag2)
    {
        $this->setScripts(array_merge($this->getScripts(), $bag2->getScripts()))
            ->setStyles(array_merge($this->getStyles(), $bag2->getStyles()))
            ;
    }
}