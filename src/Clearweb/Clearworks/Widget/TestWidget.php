<?php namespace Clearweb\Clearworks\Widget;

$root_path = realpath(dirname(__FILE__));


class TestWidget extends Widget{
	public function getName() {
		return 'test_widget';
	}
	
	public function getID() {
		return 'test.....';
	}
	
	public function getStyles() {
		return array();
	}
	
	public function getScripts() {
		return $scripts = array(
                                array('name' => 'test-widget',
                                      'url'=>'packages/clearweb/clearworks/js/test_widget.js',
                                      'dependencies' => array('jquery'),
                                      )
                                );
	}
	
	public function getView() {
		$html = '';
		$html .= '<div id="test_widget_inside">';
		
		foreach($this->getParameters() as $key => $value) {
			$html .= "<div class='$key'>".
				"$key : $value".
				'</div>';
		}
		
		$html .= '</div>';
		return $html;
	}
}