<?php namespace Clearweb\Clearworks\Widget\Exception;

class WidgetNotInitedException extends \Exception
{}