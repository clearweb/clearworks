<?php namespace Clearweb\Clearworks\Widget;

use Clearweb\Clearworks\Content\Container;
use Clearweb\Clearworks\Contracts\IViewable;

use Clearweb\Clearworks\Widget\Exception\NoNameException;

class ContainerWidget extends Widget
{
	private $container = null;
	private $additional_scripts = array();
	private $additional_styles  = array();
	private $classes   = array();    
    
	private $id;
    private $name;

    public function __construct() {
        $container = new Container;
        $this->setContainer($container);        
    }
    
	public function addViewable(IViewable $viewable, $name=null, $position=0) {
        $this->getContainer()
            ->addViewable($viewable, $name, $position)
            ;
        
        return $this;
    }
    
	public function getID() {
		return $this->id;
	}
    
    public function setID($id) {
        $this->id = $id;
        
        return $this;
    }

	public function getName() {
        if (empty($this->name)) {
            throw new NoNameException('Widget of class '.get_class($this).' has no name set!');
        }
        
		return $this->name;
	}
    
    public function setName($name) {
        $this->name = $name;
        
        return $this;
    }
	
	function setContainer(Container $container) {
		$this->container = $container;
		return $this;
	}
	
	function getContainer() {
		return $this->container;
	}
	
    function addScript($name, $url, array $dependencies=array()) {
        $this->additional_scripts[$name] = array('name'=>$name, 'url'=>$url, 'dependencies'=>$dependencies);
		return $this;
	}
	
	function addStyle($style) {
		$this->additional_styles[] = $style;
		return $this;
	}
	
	function getView()
	{
		if (empty($this->container)) {
			throw new Exception\NoContainerException('No container defined, most of the time due to missing parent::init');
		}
		
		return $this->container->getView();
	}
    
	public function addClass($class) {
		$this->classes[] = $class;
		return $this;
	}
	
	public function removeClass($class) {
		foreach($this->classes as $i => $currentClass) {
			if ($currentClass == $class) {
				unset($this->classes[$i]);
			}
		}
		
		return $this;
	}
    
	public function setClasses(array $classes) {
		$this->classes = $classes;
		return $this;
	}
	
	public function getClasses() {
		return $this->classes;
	}
    
	function getStyles()
	{
		if (empty($this->container)) {
			throw new \Content\NoContainerException('No container defined');
		}
		
		return array_merge($this->container->getStyles(), $this->additional_styles);
	}

	function getScripts()
	{
		if (empty($this->container)) {
			throw new NoContainerException('No container defined');
		}
		return array_merge($this->container->getScripts(), $this->additional_scripts);
	}
	
    
    
	function execute() {
        parent::execute();
        
		if (empty($this->container)) {
			throw new Exception\NoContainerException('No container defined, most of the time due to missing parent::init');
		}

        foreach($this->getClasses() as $class) {
            $this->getContainer()->addClass($class);
        }
        
		return $this;
	}
}