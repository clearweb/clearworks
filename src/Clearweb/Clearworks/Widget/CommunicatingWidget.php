<?php namespace Clearweb\Clearworks\Widget;

use Clearweb\Clearworks\Communication\ReloadWidget;
use Clearweb\Clearworks\Communication\ParameterInitializer;
use Clearweb\Clearworks\Communication\ParameterListener;
use Clearweb\Clearworks\Communication\ParameterChanger;

abstract class CommunicatingWidget extends ContainerWidget {
	private $listeners = array();
	
	/**
	 * Gets the variables this widget listens to.
	 * @return array an array with variable names as strings
	 */
	function getListeners() {
		return $this->listeners;
	}
	
	/**
	 * Add a listener to the list of variables we listen to.
	 * @param string $listener the variable name which we will listen to.
	 */
	function addListener($listener) {
		$this->listeners[] = $listener;
		return $this;
	}
    
	/**
	 * Remove a listener from the list of variables we listen to.
	 * @param string $listener the variable name which we will not listen to anymore.
	 */
	function removeListener($listener) {
        foreach($this->listeners as $i => $listenerCursor) {
            if( $listenerCursor == $listener) {
                unset($this->listeners[$i]);
            }
        }
        
		return $this;
	}
	
	/**
	 * generates and sets an id for the widget.
	 */
	protected function generateID() {
		return uniqid();
	}
    
	public function getURL() {
		return \Clearworks::getWidgetUrl($this, array());
	}
	
	public function init() {
        $id = $this->getId();
        
        if (empty($id)) {
            $this->setID($this->generateID());
        }
        
		parent::init();
        
		$this->addScript('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.js');
		$this->addScript('canjs', 'packages/clearweb/clearworks/js/canjs/can.jquery.js', array('jquery'));
		$this->addScript('communication', 'packages/clearweb/clearworks/js/widget_communication.js', array('jquery', 'canjs'));
		
		return $this;
	}
	
	public function execute() {
		parent::execute();
        
		$this->getContainer()
			->addViewable(
						  with(new ParameterInitializer)
						  ->setParameters($this->getParameters())
						  )
			->addViewable(
						  with(new ParameterListener)
						  ->setListeners($this->getListeners())
						  ->setChangeAction(
											with(new ReloadWidget)
											->setWidget($this)
											)
						  ->setDeduplicateId($this->getName().'-[attribute]')
						  )
			->addClass('communicating-widget')
			->addHTMLAttribute('id', 'widget-'.$this->getId())
			->addHTMLAttribute('data-update-url', $this->getURL())
			->addHTMLAttribute('data-widget-name', $this->getName())
			;
		
		return $this;
	}

    public function changeParameter($key, $value)
    {
		$this->getContainer()
			->addViewable(
                with(new ParameterChanger)
                ->setParameter($key, $value)
            )
            ;
    }
}