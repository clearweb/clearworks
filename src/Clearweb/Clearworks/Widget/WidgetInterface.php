<?php namespace Clearweb\Clearworks\Widget;

use Clearweb\Clearworks\Contracts\IViewable;
use Clearweb\Clearworks\Contracts\IExecutable;
use Clearweb\Clearworks\Contracts\IParametrizable;

interface WidgetInterface extends IViewable, IExecutable, IParametrizable
{
	/**
	 * Get the name of the widget
	 * @return the name of the widget as a string
	 */
	function getName();
	
	/**
	 * Gets the id of the widget
	 */
	function getID();
}