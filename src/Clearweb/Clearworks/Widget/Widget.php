<?php namespace Clearweb\Clearworks\Widget;

use Clearweb\Clearworks\Contracts\IViewable;
use Clearweb\Clearworks\Contracts\IExecutable;

use Clearweb\Clearworks\Widget\Exception\WidgetNotInitedException;

use Event;

abstract class Widget implements WidgetInterface {
    private $status = IExecutable::STATUS_START;
    private $shouldWrap = true;
    
	protected $parameters = array();
	
	/**
	 * Set URL parameters for the widget
	 * @return Widget this widget.
	 */
	function setParameters(array $parameters) {
		$this->parameters = $parameters;
		
		return $this;
	}
	
	/**
	 * Gets the url parameters of the widget
	 */
	function getParameters() {
		return $this->parameters;
	}
	
	/**
	 * Gets an url parameter of the widget
	 * @param string $param the name of the parameter.
	 * @param mixed $default the default value to return if the parameter is not set.
	 * @return string the value of the parameter.
	 */
	function setParameter($param, $value) {
		$this->parameters[$param] = $value;
		
		return $this;
	}
	
	/**
	 * Gets an url parameter of the widget
	 * @param string $param the name of the parameter.
	 * @param mixed $default the default value to return if the parameter is not set.
	 * @return string the value of the parameter.
	 */
	function getParameter($param, $default = null) {
		if (isset($this->parameters[$param]))
			return $this->parameters[$param];
		else
			return $default;
	}
	
    /**
     * Sets the execution status of the widget
     * @param string $status the status we are currently in
     */
    public function setStatus($status) {
        
        switch($status) {
        case IExecutable::STATUS_START:
        case IExecutable::STATUS_INITED:
        case IExecutable::STATUS_EXECUTED:
            $this->status = $status;
            break;
        default:
            $this->status = IEXECUTABLE::STATUS_UNKNOWN;
        }
        
        return $this;
    }
    
    /**
     * Gets the execution status of the widget
     * @return string the status we are currently in
     */
    public function getStatus() {
        return $this->status;
    }
    
	/**
	 * Initiates the widget (loads all things etc.)
	 * @precondition all the variables are set
	 */
	public function init() {
        $this->setStatus(IExecutable::STATUS_INITED);
        Event::fire('widget.init', array($this));
        
        return $this;
    }
	
    /**
     * Gets if object is inited
     * @return boolean if inited true, false otherwise
     */
    public function isInited()
    {
        return ($this->getStatus() != IExecutable::STATUS_START);
    }

    /**
     * Gets if object is executed
     * @return boolean if executed true, false otherwise
     */
    public function isExecuted()
    {
        return ($this->getStatus() == IExecutable::STATUS_EXECUTED);
    }
    

	/**
	 * Executes the widget (Does all calculations etc.)
	 * @precondition initiation is done
	 */
	function execute() {
        if ( ! $this->isInited()) {
            throw new WidgetNotInitedException('Widget of class '.get_class().' is executing but has not been inited');
        }
        
        Event::fire('widget.execute', array($this));
        $this->setStatus(IExecutable::STATUS_EXECUTED);
        
        return $this;
    }

    public function getShouldWrap()
    {
        return $this->shouldWrap;
    }
    
    public function setShouldWrap($shouldWrap)
    {
        $this->shouldWrap = $shouldWrap;
        
        return $this;
    }


    /* if a method is called to proceed a 
     * step in the process, make sure the steps before are
     * also done.
     */
    public function __call($method,$arguments) {
        if ($method == '__init') {

            return call_user_func_array(array($this,'init'),$arguments);
            
        } elseif ($method == '__execute') {
            
            if( ! $this->isInited()) {
                $this->init();
            }
            
            return call_user_func_array(array($this,'execute'),$arguments);
            
        } elseif($method == '__getView') {

            if( ! $this->isInited()) {
                $this->init();
            }
            
            if( ! $this->isExecuted()) {
                $this->execute();
            }
            
            return call_user_func_array(array($this,'getView'),$arguments);
        }
    }
}