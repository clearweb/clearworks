<?php namespace Clearweb\Clearworks\Traits;

use Clearweb\Clearworks\Assets\IAssetBag;

trait AssetsHolder
{
    private $assetBag = null;
    private $scripts = array();
	private $styles  = array();
    
    public function setAssetBag(IAssetBag $bag) {
        $this->assetBag = $bag;
        
        return $this;
    }
    
    public function getAssetBag() {
        return $this->assetBag;
    }
    
    public function addScript($name, $url, array $dependencies=array())
    {
        $this->scripts[$name] = array('name'=>$name, 'url'=>$url, 'dependencies'=>$dependencies);
		return $this;
	}
	
	public function addStyle($style)
    {
		$this->styles[] = $style;
		return $this;
	}
    
        public function getStyles()
    {
        return $this->styles;
    }
    
    public function setStyles($styles)
    {
        $this->styles = $styles;
        return $this;
    }
    
    public function getScripts()
    {
        return $this->scripts;
    }

    public function setScripts($scripts)
    {
        $this->scripts = $scripts;
        return $this;
    }
}