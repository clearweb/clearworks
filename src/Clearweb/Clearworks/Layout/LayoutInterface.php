<?php namespace Clearweb\Clearworks\Layout;

use Clearweb\Clearworks\Contracts\IViewable;

interface LayoutInterface extends IViewable {}