<?php namespace Clearweb\Clearworks\Layout;

abstract class LinearLayout extends ContainerLayout {
	protected function getCompatibleLocationClasses() {
		return array('Clearweb\Clearworks\Layout\Location\LinearLayoutLocation');
	}

	/**
	 * Gets the view of the container
	 * @param $container string the name of the container.
	 * @return string with html of the container.
	 */
	function getContainerView($container) {
		$widgets = array();
		$html    = '';
		
		foreach($this->widget_locations as $widget_location) {
			$layout_location = $widget_location->getLayoutLocation();
			if ($layout_location->getContainer() == $container) {
				$widgets[$layout_location->getRank()][] = $widget_location->getWidget();
			}
		}
		
		ksort($widgets);
		
		foreach($widgets as $rank => $widget_rank_widgets) {
			foreach($widget_rank_widgets as $widget_rank_widget) {
				$html .= $this->getWidgetView($widget_rank_widget);
			}
		}
		
		return $html;
	}
    
    public function getWidgetView($widget)
    {
        return $widget->__getView();
    }
}