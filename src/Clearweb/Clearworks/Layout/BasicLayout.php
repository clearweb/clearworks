<?php namespace Clearweb\Clearworks\Layout;

use Clearweb\Clearworks\Page\WidgetLocation;

abstract class BasicLayout implements LayoutInterface {
    private $additional_scripts = array();
	private $additional_styles  = array();

    function addScript($name, $url, array $dependencies=array()) {
        $this->additional_scripts[$name] = array('name'=>$name, 'url'=>$url, 'dependencies'=>$dependencies);
		return $this;
	}
	
	function addStyle($style) {
		$this->additional_styles[] = $style;
		return $this;
	}
    
	public function getStyles() {
		$styles = $this->additional_styles;
		foreach($this->getWidgets() as $widget) {
			$styles = array_merge($styles, $widget->getStyles());
		}
		return $styles;
	}
	
	public function getScripts() {
		$scripts = $this->additional_scripts;
		
		foreach($this->getWidgets() as $widget) {
			$scripts = array_merge($scripts, $widget->getScripts());
		}
		return $scripts;
	}

    abstract protected function getWidgets();
}