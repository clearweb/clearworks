<?php namespace Clearweb\Clearworks\Layout;

use Clearweb\Clearworks\Page\WidgetLocation;

/**
 * Abstract class representing layouts with containers.
 * @abstract
 */
abstract class ContainerLayout extends WidgetLayout {
	protected $containers = array();
	protected $containers_loaded = FALSE;
	
	/**
	 * Loads the containers of the layout in <var>containers</var>
	 * and sets <var>containers_loaded</var> accordingly.
	 * @abstract
	 */
	abstract protected function loadContainers();

	/**
	 * Gets the view of the container
	 * @param $container string the name of the container.
	 * @return string with html of the container.
	 * @abstract
	 */
	abstract function getContainerView($container);
	
	/**
	 * Gets the containers for the layout.
	 * @pre <var>containers_loaded</var> has correct value.
	 * @return an array with all containers of the layout.
	 */
	protected function getContainers() {
		if ( ! $this->containers_loaded) {
			$this->loadContainers();
		}
		
		return $this->containers;
	}
	
	/**
	 * Decides wether the layout location is compatible with this layout.
	 * @param $location The LayoutLocation we want to check.
	 * @return <code>TRUE</code> if it is compatible, otherwise <code>FALSE</code>
	 */
	protected function isCompatibleLayoutLocation(Location\LayoutLocation $location) {
		return (parent::isCompatibleLayoutLocation($location) && $this->hasContainer($location->getContainer()));
	}
	
	/**
	 * Checks if a container exists inside the layout.
	 * @param $container the name of the container we check for
	 * @return <code>TRUE</code> if the container exists, otherwise <code>FALSE</code>
	 */
	public function hasContainer($container) {
		return ( in_array($container, $this->getContainers()));
	}
}