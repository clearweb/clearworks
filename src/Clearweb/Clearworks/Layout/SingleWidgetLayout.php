<?php namespace Clearweb\Clearworks\Layout;

use Clearweb\Clearworks\Page\WidgetLocation;

class SingleWidgetLayout extends WidgetLayout {
	function getCompatibleLocationClasses() {
		return array('Clearweb\Clearworks\Layout\Location\LayoutLocation');
	}

	/**
	 * Builds layout and returns the view.
	 * @return View of the layout
	 */
	public function getView() {
		if (empty($this->widget_locations) || !($this->widget_locations[0] instanceof WidgetLocation)) {
			return '';
		} else {
			return $this->widget_locations[0]->getWidget()->getView();
		}
	}
	
	
}