<?php namespace Clearweb\Clearworks\Layout;

class TestLinearLayout extends LinearLayout {
	/**
	 * Dummy loading for containers.
	 */
	protected function loadContainers() {
		$this->containers_loaded = TRUE;
	}
	
	/**
	 * Returns a test view indicating the set widgets.
	 */
	public function getView() {
		$html = '<h1>Test Linear Layout!</h1>';
		
		foreach($this->widget_locations as $widget_location) {
			$html .= "{$widget_location->getLayoutLocation()->getContainer()} :<br /><h2>{$widget_location->getWidget()->getName()}</h2><br />{$widget_location->getWidget()->getView()}<br /><br />_______________________________________________________________________________";
		}
		
		return $html;
	}
	
	/**
	 * We want to show all containers in the View
	 */
	public function hasContainer($container) {
		return TRUE;
	}
}