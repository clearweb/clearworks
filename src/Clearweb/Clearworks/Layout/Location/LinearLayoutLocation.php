<?php namespace Clearweb\Clearworks\Layout\Location;

class LinearLayoutLocation extends ContainerLayoutLocation {
	protected $rank;
	
	function setRank($rank) {
		$this->rank = $rank;
		
		return $this;
	}

	function getRank() {
		return $this->rank;
	}
}