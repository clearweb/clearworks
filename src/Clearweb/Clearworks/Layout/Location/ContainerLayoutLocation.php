<?php namespace Clearweb\Clearworks\Layout\Location;

class ContainerLayoutLocation extends LayoutLocation {
	protected $container;
	
	function setContainer($container) {
		$this->container = $container;
		return $this;
	}
	
	function getContainer() {
		return $this->container;
	}
}