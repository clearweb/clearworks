<?php namespace Clearweb\Clearworks\Layout;

use Clearweb\Clearworks\Page\WidgetLocation;

abstract class WidgetLayout extends BasicLayout implements LayoutInterface {
	/**
	 * The locations of the linked widgets
	 */
	protected $widget_locations = array();
	
	/**
	 * The incompatible locations of the linked widgets
	 */
	protected $incompatible_widget_locations = array();
    
	/**
	 * Gets the compatible LayoutLocation classes.
	 * @return an array with class names of the compatible LayoutLocations
	 */
	abstract protected function getCompatibleLocationClasses();
	
	/**
	 * Decides wether the layout location is compatible with this layout.
	 * @param LayoutLocation $location The LayoutLocation we want to check.
	 * @return <code>TRUE</code> if it is compatible, otherwise <code>FALSE</code>
	 */
	protected function isCompatibleLayoutLocation(Location\LayoutLocation $location) {
		foreach($this->getCompatibleLocationClasses() as $compatible_class) {
			if (is_a($location, $compatible_class))
				return TRUE;
		}
		
		return FALSE;
	}
	
	/**
	 * Gets all the registered widgets
	 * @return Array with Widget objects
	 */
	protected function getWidgets() {
		$widgets = array();
		foreach($this->widget_locations as $widget_location) {
			$widgets[] = $widget_location->getWidget();
		}
		
		return $widgets;
	}
	
	/**
	 * Adds a widget to be loaded by the layout.
	 * @return the layout itself (for chaining purposes)
	 */
	public function addWidgetLocation(WidgetLocation $widget_location) {
		if($this->isCompatibleLayoutLocation($widget_location->getLayoutLocation()))
			$this->widget_locations[] = $widget_location;
		else
			$this->incompatible_widget_locations[] = $widget_location;
		
		return $this;
	}
	
	/**
	 * Sets the widgets to be loaded by the layout.
	 * @return the layout itself (for chaining purposes)
	 */
	public function setWidgetLocations(array $widget_locations) {
		$this->widget_locations = array();
		$this->incompatible_widget_locations = array();
		
		foreach($widget_locations as $widget_location) {
			$this->addWidgetLocation($widget_location);
		}
		
		return $this;
	}
}