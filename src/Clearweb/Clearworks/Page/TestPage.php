<?php namespace Clearweb\Clearworks\Page;

use Clearweb\Clearworks\Widget\TestWidget;
use Clearweb\Clearworks\Layout\TestLinearLayout;
use Clearweb\Clearworks\Layout\Location\LinearLayoutLocation;

/**
 * Page from the database.
 */
class TestPage implements PageInterface {

	function isSlug($slug) {return true;}
	
	function getView() {
		$layout = new TestLinearLayout();
		$widget_locations = array();
		$widget = new TestWidget;
		$widget_location = new LinearLayoutLocation;
		$widget_location->setContainer('first');
		
		$widget_locations[] = new WidgetLocation($widget_location, $widget);
		$layout->setWidgets($widget_locations);
		return $layout->getView();
	}
}