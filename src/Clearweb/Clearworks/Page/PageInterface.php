<?php namespace Clearweb\Clearworks\Page;

use Clearweb\Clearworks\Contracts\IViewable;

/**
 * An interface for pages which can be loaded by Clearworks
 */
interface PageInterface extends IViewable {
	/**
	 * checks if the slug of this page is equal to a given slug.
	 * @param $slug string the name to check this page against.
	 * @return boolean <code>TRUE</code> if slug corresponds with page,
	 * <code>FALSE</code>otherwise 
	 */
	function isSlug($slug);
	
	/**
	 * Sets the URL of the current page request (incl. variables, excl. environment)
	 * @param $url string a string with the url.
	 * @return PageInterface the current object for chaining purposes
	 */
	function loadFromURL($url);
	
	/**
	 * Initiates the page
	 */
	function init();
	
	/**
	 * Executes the code of the page
	 */
	function execute();
}