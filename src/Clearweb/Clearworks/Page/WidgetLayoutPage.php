<?php namespace Clearweb\Clearworks\Page;

use Clearweb\Clearworks\Layout\WidgetLayout;

class WidgetLayoutPage extends LayoutPage {
	/**
	 * The locations of the linked widgets
	 */
	protected $widget_locations = array();
	
	/**
	 * Adds a widget to the page
	 * @return WidgetLayoutPage the page itself (for chaining purposes)
	 */
	public function addWidgetLocation(WidgetLocation $widget_location) {
		$this->widget_locations[] = $widget_location;
		
		return $this;
	}
	
	protected function getCompatibleLayoutClasses() {
		return array('Clearweb\Clearworks\Layout\WidgetLayout');
	}
	
	/**
	 * Sets the widgets which are attached into the page.
	 * @return WidgetLayoutPage the page itself (for chaining purposes)
	 */
	public function setWidgetLocations(array $widget_locations) {
		$this->widget_locations = $widget_locations;
		
		return $this;
	}
	
	/**
	 * Gets the View of the page
	 * @return View the view of the page
	 */
	public function getView() {
		$this->layout->setWidgetLocations($this->widget_locations);
		return parent::getView();
	}
	
	public function init() {
		parent::init();
		foreach($this->widget_locations as $widget_location) {
			$widget = $widget_location->getWidget();
			$widget->setParameters($this->getParameters());
			$widget->__init();
		}
	}
	
	public function execute() {
		
		parent::execute();
		foreach($this->widget_locations as $widget_location) {
			$widget = $widget_location->getWidget();
			$widget->__execute();
		}
	}
}