<?php namespace Clearweb\Clearworks\Page;

use Clearweb\Clearworks\Layout\Location\LinearLayoutLocation;
use Clearweb\Clearworks\Widget\WidgetInterface;

class LinearPage extends WidgetLayoutPage {
	public function addWidgetLinear(WidgetInterface $widget, $container, $rank = 1) {
		return $this->addWidgetLocation(
            new WidgetLocation(
                with(new LinearLayoutLocation)
                ->setRank($rank)
                ->setContainer($container),
                $widget
            )
        );
	}
	
	protected function getCompatibleLayoutClasses() {
		return array('Clearweb\Clearworks\Layout\LinearLayout');
	}
}