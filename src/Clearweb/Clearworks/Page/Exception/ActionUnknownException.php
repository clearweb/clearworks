<?php namespace Clearweb\Clearworks\Page\Exception;

class ActionUnknownException extends \Exception {}