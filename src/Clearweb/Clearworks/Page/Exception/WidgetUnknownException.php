<?php namespace Clearweb\Clearworks\Page\Exception;

class WidgetUnknownException extends \Exception {}