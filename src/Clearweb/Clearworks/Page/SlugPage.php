<?php namespace Clearweb\Clearworks\Page;

abstract class SlugPage implements PageInterface {
	protected $slug = '';
	protected $parameters = array();
	
	/**
	 * Gets the slug.
	 * @return string the slug.
	 */
	public function getSlug() {
		return $this->slug;
	}
	
	/**
	 * Sets the slug.
	 * @param $slug string the slug.
	 * @return SlugPage this page.
	 */
	public function setSlug($slug) {
		$this->slug = $slug;
		
		return $this;
	}
		
	/**
	 * checks if the slug of this page is equal to a given slug.
	 * @param $slug string the name to check this page against.
	 * @return boolean <code>TRUE</code> if slug corresponds with page,
	 * <code>FALSE</code>otherwise 
	 */
	function isSlug($slug) {
		return ($slug == $this->getSlug());
	}
	
	/**
	 * Sets the URL of the current page request (incl. variables, excl. environment)
	 * @param $url string a string with the url.
	 * @return PageInterface the current object for chaining purposes
	 */
	function loadFromURL($url) {
		$url_params = explode('/', $url);
		array_shift($url_params);
		
		while( ! is_null($key = array_shift($url_params))) {
			$value = array_shift($url_params);
			$this->setParameter($key, $value);
		}
		
		return $this;
	}
	
	/**
	 * Set a parameter
	 * @param $key string the key of the parameter
	 * @param $value string the value of the parameter
	 * @return PageInterface the current object for chaining purposes
	 */
	function setParameter($key, $value=null) {
		$this->parameters[$key] = $value;
		return $this;
	}
	
	function removeParameter($key) {
		if (isset($this->parameters[$key]))
			unset($this->parameters[$key]);
	}
	
	/**
	 * Get all parameters
	 * @return array an array with all parameters.
	 */
	function getParameters() {
		return $this->parameters;
	}
	
	/**
	 * Get a parameter
	 * 
	 * @return string|NULL the parameter value
	 */
	function getParameter($key, $default=NULL) {
		if (isset($this->parameters[$key])) {
			return $this->parameters[$key];
		} else {
			return $default;
		}
	}
	
	function init() {}
	
	function execute() {}
}