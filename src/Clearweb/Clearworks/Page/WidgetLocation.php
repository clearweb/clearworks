<?php namespace Clearweb\Clearworks\Page;

class WidgetLocation {
	protected $layout_location;
	protected $widget;

	function __construct($layout_location, $widget) {
		$this->layout_location = $layout_location;
		$this->widget = $widget;
	}

	function setLayoutLocation($layout_location) {
		$this->layout_location = $layout_location;
		return $this;
	}

	function setWidget($widget) {
		$this->widget = $widget;
		return $this;
	}
	
	function getLayoutLocation() {
		return $this->layout_location;
	}
	
	function getWidget() {
		return $this->widget;
	}
}