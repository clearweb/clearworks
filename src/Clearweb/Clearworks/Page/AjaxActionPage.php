<?php namespace Clearweb\Clearworks\Page;

use Clearweb\Clearworks\Page\Exception\ActionUnknownException;

/**
 * class representing a page with one action (can be used to execute an action through ajax)
 */
class AjaxActionPage extends SlugPage {
	protected $action = null;

	function __construct() {
		$this->setSlug('action');
	}
	
	function getStyles() {
		return array();
	}
	
	function getScripts() {
		return array();
	}
	
	function init() {
		$this->action->setParameters($this->getParameters());
		$this->action->init();
	}
	
	function execute() {
		$this->action->execute();
	}
	
	/**
	 * Gets view for the action
	 */
	function getView() {
		return json_encode($this->action->getJSON());
	}
	
	//	3.858,00
	
	/**
	 * In an ajax action url, the first param is the action class.
	 * @param $url string a string with the url.
	 * @return PageInterface the current object for chaining purposes
	 * @todo add Exception here when URL is not valid
	 */
	function loadFromURL($url) {
		$url_params = explode('/', $url);
		array_shift($url_params);
		
		$action_class  = urldecode(array_shift($url_params));
		
		if (is_subclass_of($action_class, 'Clearweb\Clearworks\Action\IAction')) {
		
			$this->action = new $action_class;
			
			while( ! is_null($key = array_shift($url_params))) {
				$value = array_shift($url_params);
				$this->setParameter($key, $value);
			}
		} else {
			throw new ActionUnknownException("Action '$action_class' is unknown");
		}
		
		return $this;
	}
}