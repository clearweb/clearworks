<?php namespace Clearweb\Clearworks\Page;

interface PageProviderInterface {
	
	/**
	 * checks if a page exist in provider
     * @param string $slug the slug of the page
     * @return boolean <code>true</code> if the page exists, otherwise <code>false</code>
	 */
	public function pageExists($slug);

	/**
	 * get the the page
     * @param string $slug the slug of the page
	 * @return PageInterface a page or <code>null</code>
	 */
	public function getPage($slug);
}