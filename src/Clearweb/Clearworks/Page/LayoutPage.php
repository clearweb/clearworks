<?php namespace Clearweb\Clearworks\Page;

use Clearweb\Clearworks\Layout\LayoutInterface;

class LayoutPage extends SlugPage {
	private $scripts = array();
	private $styles  = array();
    
	/**
	 * Sets the layout.
	 * @param Layout $layout the layout.
	 * @return LayoutPage this page.
	 */
	function setLayout(LayoutInterface $layout) {
		if ($this->isCompatibleLayout($layout)) {
			$this->layout = $layout;
		} else {
			throw new Exception('The layout is not compatible with this page!');
		}
		
		return $this;
	}
	
	/**
	 * Decides wether the layout is compatible with this page.
	 * @param LayoutInterface $layout The Layout we want to check.
	 * @return <code>TRUE</code> if it is compatible, otherwise <code>FALSE</code>
	 */
	protected function isCompatibleLayout(LayoutInterface $layout) {
		foreach($this->getCompatibleLayoutClasses() as $compatible_class) {
			if (is_a($layout, $compatible_class))
				return TRUE;
		}
		
		return FALSE;
	}
	
	/**
	 * Gets the compatible layout classes.
	 * @return array an array with class names of the compatible layouts
	 */
	protected function getCompatibleLayoutClasses() {
		return array('Clearweb\Clearworks\Layout\LayoutInterface');
	}
	
	/**
	 * Gets the layout
	 * @param Layout $layout the layout of this page.
	 */
	function getLayout() {
		return $this->layout;
	}
    
    function getStyles()
	{
		return $this->styles;
	}

	function getScripts()
	{
		return $this->scripts;
	}
    
    function addScript($name, $url, array $dependencies=array()) {
		$this->scripts[$name] = array('name'=>$name, 'url'=>$url, 'dependencies'=>$dependencies);
		return $this;
	}
	
	function addStyle($style) {
		$this->styles[] = $style;
		return $this;
	}
    
    public function execute()
    {
        foreach($this->getScripts() as $script) {
            $this->layout->addScript($script['name'], $script['url'], $script['dependencies']);
        }
        
        foreach($this->getStyles() as $style) {
            $this->layout->addStyle($style);
        }
        
        return parent::execute();
    }
    
	/**
	 * Gets the View of the page
	 * @return View the view of the page
	 */
	public function getView() {
		return $this->layout->getView();
	}
    
}