<?php namespace Clearweb\Clearworks\Page;

use Clearweb\Clearworks\Page\Exception\WidgetUnknownException;

/**
 * class representing a page with one widget (can be used to load widget through ajax)
 */
class AjaxWidgetPage extends SlugPage {
	protected $widget = null;

	function __construct() {
		$this->setSlug('widget');
	}
	
	function getStyles() {
		return $this->widget->getStyles();
	}
	
	function getScripts() {
		return $this->widget->getScripts();
	}
	
	function init() {
		$this->widget->setParameters($this->getParameters());
		$this->widget->init();
	}
	
	function execute() {
		$this->widget->execute();
	}
	
	/**
	 * Gets view for the one-widget-page
	 */
	function getView() {
		return $this->widget->getView();
	}
	
	/**
	 * In an ajax widget page, the first param is the widget class.
	 * @param $url string a string with the url.
	 * @return PageInterface the current object for chaining purposes
	 * @todo add Exception here when URL is not valid
	 */
	function loadFromURL($url) {
		$url_params = explode('/', $url);
		array_shift($url_params);
		
		$widget_class  = urldecode(array_shift($url_params));

		if (is_subclass_of($widget_class, 'Clearweb\Clearworks\Widget\Widget')) {
			$this->widget = new $widget_class;
		
			while( ! is_null($key = array_shift($url_params))) {
				$value = array_shift($url_params);
				$this->setParameter($key, $value);
			}
		} else {
			throw new WidgetUnknownException("Widget '$widget_class' is unknown");
		}
		
		return $this;
	}
}