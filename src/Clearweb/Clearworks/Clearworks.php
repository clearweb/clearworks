<?php namespace Clearweb\Clearworks;

use Clearweb\Clearworks\Page\AjaxWidgetPage;
use Event;

class Clearworks {
	protected $environments = array();

	/**
	 * The current environment
	 */
	protected $environment = null;
    
        /**
         * The default environment
         */
        protected $default = null;
    
	/**
	 * Makes an Environment and register it.
	 * @param $name string the name of the new environment.
	 * @param $url string the url of the new environment.
         * @param $default boolean is it the default environment or not
	 * @return Mixed the made environment if success, otherwise <code>FALSE</code>
	 */
	function make($name, $url='page', $default = false) {
		$environment = with(new Environment(Event::getFacadeRoot()))->setName($name)->setSlug($url);
		if ($this->addEnvironment($environment, $default)) {
			return $environment;
		} else {
			return FALSE;
		}
	}

	/**
	 * Gets an already made environment.
	 * @param $name string with name of the required environment.
	 * @return Mixed the environment object with the specified name.
	 * If not found, return <code>FALSE</code>
	 */
	function getEnvironment($name) {
		foreach($this->environments as $environment) {
			if ($environment->getName() == $name) {
				return $environment;
			}
		}

		return FALSE;
	}

	/**
	 * Gets all registered environments.
	 * @return array with environments.
	 */
	public function getEnvironments() {
		return $this->environments;
	}
	
	/**
	 * Gets the current environment.
	 * @return mixed The environment if now using Clearworks, otherwise <code>NULL</code>
	 */
	public function getCurrentEnvironment() {
		return $this->environment;
	}
	
	/**
	 * Set the current environment
	 * @param Environment the environment to set as current environment
	 * @return Clearworks
	 */
	public function setCurrentEnvironment(Environment $environment) {
		$this->environment = $environment;
		return $this;
	}
	
	/**
	 * Gets the current page.
	 * @return mixed The page if now using Clearworks, otherwise <code>NULL</code>
	 */
	public function getCurrentPage() {
		if (is_null($this->environment)) {
			$page = NULL;
		} else {
			$page = $this->environment->getCurrentPage();
		}
		
		return $page;
	}
    
        public function getDefaultEnvironment() {
                if (is_null($this->default)) {
                        $env = null;
                } else {
                        $env = $this->default;
                }
        
                return $env;
        }
    
	/**
	 * Registers environment.
	 * @param $environment Environment the environment to register
	 * @return boolean <code>TRUE</code> if the name of the environment is not duplicate,
	 * otherwise return <code>FALSE</code>
	 */
	public function addEnvironment(Environment $environment, $default = false) {
		if ($this->getEnvironment($environment->getName()) === FALSE) {
			$this->environments[] = $environment;
            
                        if ($default)
				$this->default = $environment;
            
			return TRUE;
		}

		return FALSE;
	}

	/**
	 * Replace a registered environment if it exists (add it anyway).
	 * @param $environment Environment the environment to register
	 * @return void
	 */
	public function replaceEnvironment(Environment $environment) {
		$old_env = $this->getEnvironment($environment->getName());
		if ($old_env !== FALSE) {
			$this->removeEnvironment($old_env->getName());
		}
		
		$this->addEnvironment($environment);
	}

	/**
	 * Remove a registered environment if it exists.
	 * @param $environment_name string The name of the environment
	 */
	public function removeEnvironment($environment_name) {
		foreach($this->environments as $key => $environment) {
			if ($environment_name == $environment->getName()) {
				unset($this->environments[$key]);
			}
		}
	}
	
	/**
	 * Gets the url for an environment
	 * @param Environment $env the environment to link to
	 * @return string the url of the environmetn. Always ends with a slash.
	 */
	public function getEnvironmentUrl(Environment $env) {
		return '/'.$env->getSlug().'/';
	}
	
	/**
	 * Gets the url for a page
	 * @param Page\PageInterface $page the page to link to
	 * @param mixed $env the Environment instance to use or
	 * <code>NULL</code> if current environment should be used.
	 * @return string the url of the page. Always ends with a slash.
	 */
	public function getPageUrl(Page\PageInterface $page, $parameters = NULL, $env = NULL) {
		if (is_null($env)){
			$env = $this->getCurrentEnvironment();
		}
		
		if (is_null($parameters))
			$parameters = $page->getParameters();
		
		$params = '';
		foreach($parameters as $key=>$param) {
            if (is_array($param)) {
                $param = implode(';', $param);
            }
            
			$params .= "/$key/$param";
		}
        
                $env_slug = ($env == $this->getDefaultEnvironment())?'':'/'.$env->getSlug();
        
		return $env_slug.'/'.$page->getSlug().$params;
	}	
	
	/**
	 * Gets the url for a widget.
	 * @param Widget\Widget $widget the widget to get the URL of.
	 * @param mixed $parameters the parameters or NULL to use widget parameters
	 * @param mixed $env the Environment instance to use or
	 * <code>NULL</code> if current environment should be used.
	 * @return string the url of the widget. Always ends with a slash.
	 */
	public function getWidgetUrl(Widget\Widget $widget, $parameters = NULL, $env = NULL) {
		if (is_null($env)){
			$env = $this->getCurrentEnvironment();
		}
		
		if (is_null($parameters))
			$parameters = $widget->getParameters();
		
		$params = '';
		foreach($parameters as $key=>$param) {
            if ( ! empty($key)) {
                if (is_array($param)) {
                    $param = implode(';', $param);
                }
                $params .= "/$key/$param";
            }
		}
        
        return $this->getPageUrl(new AjaxWidgetPage, array()).'/'.urlencode(get_class($widget)).$params;
	}

	/**
	 * Gets the url for an action.
	 * @param Action\IAction $action the action to get the URL of.
	 * @param mixed $parameters the parameters or NULL to use action parameters
	 * @param mixed $env the Environment instance to use or
	 * <code>NULL</code> if current environment should be used.
	 * @return string the url of the action. Always ends with a slash.
	 */
	public function getActionUrl(Action\IAction $action, $parameters = NULL, $env = NULL) {
		if (is_null($env)){
			$env = $this->getCurrentEnvironment();
		}
		
		if (is_null($parameters))
			$parameters = $action->getParameters();
		
		$params = '';
		foreach($parameters as $key=>$param) {
			$params .= "/$key/$param";
		}
		
		return '/'.$env->getSlug().'/action/'.urlencode(get_class($action)).$params;
	}
}