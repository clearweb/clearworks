<?php namespace Clearweb\Clearworks\Action;

use Clearweb\Clearworks\Action\IAction;
use Clearweb\Clearworks\Contracts\IExecutable;

abstract class Action implements IAction{
    /* - Executable - */
    
    private $status;
    
    /**
     * Sets the execution status of the widget
     * @param string $status the status we are currently in
     */
    public function setStatus($status) {
        
        switch($status) {
        case IExecutable::STATUS_START:
        case IExecutable::STATUS_INITED:
        case IExecutable::STATUS_EXECUTED:
            $this->status = $status;
        default:
            $this->status = IExecutable::STATUS_UNKNOWN;
        }
        
        return $this;
    }
    
    /**
     * Gets the execution status of the widget
     * @return string the status we are currently in
     */
    public function getStatus() {
        return $this->status;
    }
    
	/**
	 * Initiates the widget (loads all things etc.)
	 * @precondition all the variables are set
	 */
	public function init() {
        $this->setStatus(IExecutable::STATUS_INITED);
        
        return $this;
    }
	
    /**
     * Gets if object is inited
     * @return boolean if inited true, false otherwise
     */
    public function isInited()
    {
        return ($this->getStatus() != IExecutable::STATUS_OPEN);
    }

    /**
     * Gets if object is executed
     * @return boolean if executed true, false otherwise
     */
    public function isExecuted()
    {
        return ($this->getStatus() == IExecutable::STATUS_EXECUTED);
    }
        

	/* - Parametrizable - */
	private $parameters = array();
	/**
	 * Set URL parameters for the widget
	 * @return Widget this widget.
	 */
	function setParameters(array $parameters) {
		$this->parameters = $parameters;
		
		return $this;
	}
	
	/**
	 * Gets the url parameters of the widget
	 */
	function getParameters() {
		return $this->parameters;
	}
	
	/**
	 * Set a parameter
	 * @param $key string the key of the parameter
	 * @param $value string the value of the parameter
	 * @return PageInterface the current object for chaining purposes
	 */
	function setParameter($key, $value=null) {
		$this->parameters[$key] = $value;
		return $this;
	}
	
	function removeParameter($key) {
		if (isset($this->parameters[$key]))
			unset($this->parameters[$key]);
	}
	
	/**
	 * Get a parameter
	 * 
	 * @return string|NULL the parameter value
	 */
	function getParameter($key, $default=NULL) {
		if (isset($this->parameters[$key])) {
			return $this->parameters[$key];
		} else {
			return $default;
		}
	}
	
}