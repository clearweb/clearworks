<?php namespace Clearweb\Clearworks\Action;

/**
 * This class represents all anchors (having urls).
 */
class ActionAnchor extends ActionButton {
	private $url;
	
	public function setURL($url) {
		$this->url = $url;
		return $this;
	}
	
	public function getURL() {
		return $this->url;
	}
	
	public function getView() {
		$attributes = '';
		foreach($this->getHTMLAttributes() as $key=>$value) {
			$attributes .= ' '.$key.'='.json_encode($value).'';
		}
		return '<a'.$attributes.' class="'.implode(' ', $this->getClasses()).'" href="'.$this->getURL().'">'.$this->getTitle().'</a>';
	}
    
	public function init() {}
	public function execute() {}
}