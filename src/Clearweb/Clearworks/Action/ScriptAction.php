<?php namespace Clearweb\Clearworks\Action;

use Clearweb\Clearworks\Contracts\IViewable;

abstract class ScriptAction implements IViewable
{
    private $additional_scripts = array();
	private $additional_styles  = array();
    
	abstract public function getActionScript();
    
	
    function addScript($name, $url, array $dependencies=array()) {
        $this->additional_scripts[$name] = array('name'=>$name, 'url'=>$url, 'dependencies'=>$dependencies);
		return $this;
	}
	
	function addStyle($style) {
		$this->additional_styles[] = $style;
		return $this;
	}
	
    function getScripts()
    {
        return $this->additional_scripts;
    }
    
    function getStyles()
    {
        return $this->additional_styles;
    }
    
    function getView()
	{
		$html = '<script type="text/javascript">'.PHP_EOL;
		//$html .= 'context=$(this);'.PHP_EOL;
		$html .= $this->getActionScript().PHP_EOL;
		$html .= '</script>'.PHP_EOL;
		
		//echo htmlentities($html).'<br /><br />';
		return $html;
	}
}