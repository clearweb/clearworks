<?php namespace Clearweb\Clearworks\Action;

class ScriptActionButton extends ActionButton {
	private $script_action = null;
	
	public function getScriptAction() {
		return $this->script_action;
	}
	
	public function setScriptAction(ScriptAction $script_action) {
		$this->script_action = $script_action;
		return $this;
	}
	
	public function getView() {
		$attributes = '';
		foreach($this->getHTMLAttributes() as $key=>$value) {
			$attributes .= ' '.$key.'='.json_encode($value).'';
		}
		return '<a'.$attributes.' class="'.implode(' ', $this->getClasses()).'" href="#" onclick="'.$this->getScriptAction()->getActionScript().'; return false; ">'.$this->getTitle().'</a>';
	}
    
	public function init() {}
	public function execute() {}
}