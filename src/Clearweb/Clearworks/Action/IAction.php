<?php namespace Clearweb\Clearworks\Action;

use \Clearweb\Clearworks\Contracts\IExecutable;
use \Clearweb\Clearworks\Contracts\IParametrizable;

interface IAction extends IExecutable, IParametrizable {
	function getJSON();
}