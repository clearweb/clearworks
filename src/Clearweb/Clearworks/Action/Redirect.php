<?php namespace Clearweb\Clearworks\Action;

class Redirect extends ScriptAction
{
	private $location = '/';
	
	public function getActionScript()
	{
		return "window.location='".$this->getLocation()."'";
	}
	
	public function getLocation()
	{
		return $this->location;
	}
	
	public function setLocation($location)
	{
		$this->location = $location;
		return $this;
	}
}