<?php namespace Clearweb\Clearworks\Action;

/**
 * This class gives a basic implementation of action buttons.
 */
abstract class ActionButton implements IActionButton {
	private $parameters = array();
	private $classes    = array();
	private $html_attr  = array();
	private $additional_scripts = array();
	private $additional_styles  = array();
	
	private $title      = '';
	
	public function addClass($class) {
		if ( ! in_array($class, $this->classes))
			$this->classes[] = $class;
		
		return $this;
	}
	
	public function setClasses(array $classes) {
		$this->classes = $classes;
		return $this;
	}
	
	public function getClasses() {
		return $this->classes;
	}
	
	public function addHTMLAttribute($attr, $value) {
		$this->html_attr[$attr] = $value;
		return $this;
	}
	
	public function getHTMLAttributes() {
		return $this->html_attr;
	}
	
	public function getTitle() {
		return $this->title;
	}
	
	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

    function addScript($name, $url, array $dependencies=array()) {
        $this->additional_scripts[$name] = array('name'=>$name, 'url'=>$url, 'dependencies'=>$dependencies);
		return $this;
	}
	
	function addStyle($style) {
		$this->additional_styles[] = $style;
		return $this;
	}
	
    function getScripts()
    {
        return $this->additional_scripts;
    }
    
    function getStyles()
    {
        return $this->additional_styles;
    }
    
	public function setParameters(Array $params) {
		$this->parameters = $params;
		return $this;
	}
	
	public function setParameter($key, $value=null) {
		$this->parameters[$key] = $value;
		return $this;
	}
	
	public function getParameters() {
		return $this->parameters;
	}
	
	public function getParameter($param, $default = null) {
		if (isset($this->parameters[$param]))
			return $this->parameters[$param];
		else
			return $default;
	}
}