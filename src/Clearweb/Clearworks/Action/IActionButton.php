<?php namespace Clearweb\Clearworks\Action;

interface IActionButton extends \Clearweb\Clearworks\Contracts\IViewable,
\Clearweb\Clearworks\Contracts\IParametrizable
{
	public function init();
	public function execute();
}