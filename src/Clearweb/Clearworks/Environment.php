<?php namespace Clearweb\Clearworks;

use Clearweb\Clearworks\Page\PageInterface;
use Clearweb\Clearworks\Page\PageProviderInterface;

use Illuminate\Events\Dispatcher;

class Environment {
	
	/**
	 * A list with all subscribed PageProvider instances.
	 */
	protected $subscribed_page_providers = array();
	
	/**
	 * A list with all subscribed Page instances.
	 */
	protected $subscribed_pages = array();
	
	/**
	 * The current page. Null if none.
	 */
	protected $page = null;
	
	private $dispatcher = null;
	
	/**
	 * The default page. Null if none.
	 */
	protected $default = null;
	
	public function __construct(Dispatcher $dispatcher) {
		$this->setDispatcher($dispatcher);
	}
	
	/**
	 * Sets the event dispatcher we will use to fire events
	 * @param \Illuminate\Events\Dispatcher $dispatcher
	 */
	public function setDispatcher(Dispatcher $dispatcher)
	{
		$this->dispatcher = $dispatcher;
		
		return $this;
	}
	
	/**
	 * Gets the event dispatcher we will use to fire events
	 */
	public function getDispatcher()
	{
		return $this->dispatcher;
	}
	
	
	/**
	 * Loads the page from url if it exists.
	 * @param string $url A string with asked page
	 * @todo throw exception if page does not exist.
	 * @return Environment this environment
	 */
	public function loadFromURL($url) {
		$this->page = $this->deducePageFromUrl($url);
		
		return $this;
	}
	
	/**
	 * Finds the page which corresponds with page URL
	 * @param string $url with page url (url without params and environment path)
	 * @return PageInterface the page if it exists, otherwise <code>NULL</code>
	 */
	protected function deducePageFromUrl($url='') {
		if ((empty($url) || $url == '/') && ! is_null($this->default)) {
			return $this->default->loadFromUrl($url);
		} else {
			if (strpos($url, '/') === FALSE) {
				$page = $url;
			} else {
				$url_params = explode('/', $url);
				$page = $url_params[0];
			}
            
			foreach($this->subscribed_pages as $subscribed_page) {
				if ($subscribed_page->isSlug($page))
					return $subscribed_page->loadFromURL($url);
			}
			
			foreach($this->subscribed_page_providers as $subscribed_page_provider) {
				if ($subscribed_page_provider->pageExists($page))
					return $subscribed_page_provider->getPage($page)->loadFromURL($url);
			}
		}
		
		return NULL;
	}
	
	/**
	 * Gets the current page
	 * @return PageInterface the current page if exists, otherwise <code>NULL<code>
	 */
	public function getCurrentPage() {
		return $this->page;
	}
	
	/**
	 * Gets the name of the environment
	 * @return string with name of the environment
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Gets the slug of the environment
	 * @return string with slug of the environment
	 */
	public function getSlug() {
		return $this->slug;
	}
	
	/**
	 * Sets the name of the environment
	 * @param $name string with name of environment.
	 * @return Environment this environment.
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * Sets the slug of the environment
	 * @param $slug string with slug of environment.
	 * @return Environment this environment.
	 */
	public function setSlug($slug) {
		$this->slug = $slug;
		return $this;
	}
	
	/**
	 * Subscribe a page instance.
	 * @param PageInterface $page the page to add
	 * @param boolean $default if this should be the default page.
	 * @return <code>TRUE<code> if the page is subscibed,
	 * otherwise <code>FALSE</code>
	 */
	public function addPage(PageInterface $page, $default = FALSE) {
		if (in_array($page, $this->subscribed_pages)) {
			return FALSE;
		} else {
			if ($default)
				$this->default = $page;
			$this->subscribed_pages[] = $page;
		}
	}
	
	/**
	 * Subscribe a page provider instance.
	 * @return <code>TRUE<code> if the page provider is already subscibed,
	 * otherwise <code>FALSE</code>
	 */
	public function addPageProvider(PageProviderInterface $page_provider) {
		if (in_array($page_provider, $this->subscribed_page_providers)) {
			return FALSE;
		} else {
			$this->subscribed_page_providers[] = $page_provider;
		}
	}
	
    public function foundPage() {
		return (!is_null($this->getCurrentPage()));
    }
    
    /**
	 * initializes the environment
	 * @return Environment this
     */
    public function init() {
        $this->getDispatcher()->fire('env.init');
        
        return $this;
    }
    
    /**
     * initializes and executes the page
     * @return Environment this
     */
    public function execute() {
        $this->getDispatcher()->fire('env.execute');
        
        if ($this->foundPage()) {
			$page = $this->getCurrentPage();
			$page->init();
			$page->execute();
        }
        
        return $this;
    }
    
	/**
	 * get the view
	 * @return View page or <code>false</code> if the 
	 * page does not exist
	 */
	public function getView() {
        if ($this->foundPage()) {
			return $this->getCurrentPage()->getView();
        } else {
            return false;
        }
	}
}