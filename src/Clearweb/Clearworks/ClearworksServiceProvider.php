<?php namespace Clearweb\Clearworks;

use Illuminate\Support\ServiceProvider;

/**
 * This file is to communicate with Laravel
 */

class ClearworksServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('clearweb/clearworks');
        
        $this->app->booting(function() {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Clearworks', 'Clearweb\Clearworks\Facades\Clearworks');
        });
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$clearworks = new Clearworks();
		
		$this->app['clearworks'] = $this->app->share(function($app) use ($clearworks){
				return $clearworks;
			});
		
		\App::before(function($request) use ($clearworks) {
				foreach($clearworks->getEnvironments() as $environment) {
					$environment->addPage(new Page\AjaxWidgetPage());
					$environment->addPage(new Page\AjaxActionPage());
					
					\Route::any('/'.$environment->getSlug().'/{all?}',
							   function($url = '') use ($environment) {
									\Clearworks::setCurrentEnvironment($environment);
                                    
                                    $environment->init();
                                    
									$environment->loadFromURL($url);
                                    
									if ($environment->foundPage()) {
                                        $environment->execute();
                                        
                                        $page_result = $environment->getView();
                                        
                                        return $page_result;
									} else {
										\App::abort(404);
									}
								})->where('all', '.*');
				}
                
                $defaultEnvironment = $clearworks->getDefaultEnvironment();
                
                if ( ! empty($defaultEnvironment)) {
                    \Route::any('/{all?}',
                                function($url = '') use ($defaultEnvironment) {
									\Clearworks::setCurrentEnvironment($defaultEnvironment);
                                    
                                    $defaultEnvironment->init();
                                    
									$defaultEnvironment->loadFromURL($url);
                                    
                                    if ($defaultEnvironment->foundPage()) {
                                        $defaultEnvironment->execute();
                                        
                                        $page_result = $defaultEnvironment->getView();
                                        return $page_result;
                                    } else {
										\App::abort(404);
									}
								})->where('all', '.*');
                }
                
            });
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}