<?php namespace Clearweb\Clearworks\Contracts;

interface IExecutable {
    
    const STATUS_START    = 'start';
    const STATUS_INITED   = 'inited';
    const STATUS_EXECUTED = 'executed';
    const STATUS_UNKNOWN  = 'unknown';
    
    
	public function init();
	public function execute();
    
    public function isInited();
    public function isExecuted();
}