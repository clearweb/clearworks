<?php namespace Clearweb\Clearworks\Contracts;

interface IViewable {
	/**
	 * Gets the html.
	 * @return string with HTML markup
	 */
	public function getView();
	/**
	 * Gets the scripts needed.
	 * @return Array of strings with locations of the scripts
	 */
	public function getScripts();
	/**
	 * Gets the stylesheets needed.
	 * @return Array of strings with locations of the stylesheets
	 */
	public function getStyles();
}