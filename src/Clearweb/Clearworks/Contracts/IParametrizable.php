<?php namespace Clearweb\Clearworks\Contracts;

/**
 * todo document
 */
interface IParametrizable {
	/**
	 * Get all parameters
	 * @return array an array with all parameters.
	 */
	function getParameters();
	
	/**
	 * Set parameters
	 * @return Object the current object for chaining purposes
	 */
	function setParameters(array $params);
	
	/**
	 * Gets a parameter
	 * @param $key string the key of the parameter
	 * @param $value string the default value to return
	 * when parameter is not there.
	 */
	function getParameter($key, $default);
	
	/**
	 * Set a parameter
	 * @param $key string the key of the parameter
	 * @param $value string the value of the parameter
	 * @return Object the current object for chaining purposes
	 */
	function setParameter($key, $value);
}