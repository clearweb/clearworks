<?php namespace Clearweb\Clearworks\Communication;

use Clearweb\Clearworks\Widget\Widget;
use Clearweb\Clearworks\Action\ScriptAction;

class ReloadWidget extends ScriptAction
{
	private $widget;
	
	function setWidget(Widget $widget)
	{
		$this->widget = $widget;
		return $this;
	}
	
	function getWidget()
	{
		return $this->widget;
	}
	
	function getActionScript() {
		return "updateWidget('{$this->getWidget()->getName()}');";
	}
}