<?php namespace Clearweb\Clearworks\Communication;

use \Clearweb\Clearworks\Action\ScriptAction;

class ParameterListener extends ScriptAction
{
	private $listeners = array();
	private $deduplicate_id = null;
	
	function setListeners(array $listeners)
	{
		$this->listeners = $listeners;
		return $this;
	}
	
	function getListeners()
	{
		return $this->listeners;
	}
	
	public function setChangeAction(ScriptAction $action) {
		$this->change_action = $action;
		return $this;
	}
	
	public function getChangeAction() {
		return $this->change_action;
	}
	
	/**
	 * Sets the unique id which will ensure that the listener will not be added twice.
	 * @param string $deduplicate_id You can use the placeholder [attribute] which will be replaced by the attribute you listen to.
	 */
	public function setDeduplicateId($deduplicate_id) {
		$this->deduplicate_id = $deduplicate_id;
		return $this;
	}
	
	public function getDeduplicateId() {
		return $this->deduplicate_id;
	}
	
	public function getActionScript()
	{
		$script = '';
		foreach($this->getListeners() as $listener) {
			if (is_null($this->getDeduplicateId())) {
				$deduplicate_id = str_replace('[attribute]', $listener, $this->getDeduplicateId());
				$script .= 'subscribeToPageState("'.$listener.'", function() {'.$this->getChangeAction()->getActionScript().' }, "'.$deduplicate_id.'");';
			} else {
				$script .= 'subscribeToPageState("'.$listener.'", function() {'.$this->getChangeAction()->getActionScript().' });';
			}
		}
		
		return $script;
	}
}