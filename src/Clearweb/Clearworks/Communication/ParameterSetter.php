<?php namespace Clearweb\Clearworks\Communication;

class ParameterSetter extends ParameterChanger
{
	public function getActionScript()
	{
		$script = '';
		foreach($this->getParameters() as $key => $value) {
			$script .= 'initPageState(\''.$key.'\', \''.$value.'\');';
		}
		
		return $script;
	}
}