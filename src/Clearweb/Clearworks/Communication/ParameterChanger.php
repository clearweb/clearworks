<?php namespace Clearweb\Clearworks\Communication;

use \Clearweb\Clearworks\Contracts\IParametrizable;
use \Clearweb\Clearworks\Action\ScriptAction;

class ParameterChanger extends ScriptAction implements IParametrizable
{
	private $parameters = array();
	
	/* -- Begin parameter handling -- */
	
	function setParameter($key, $value=null) {
		$this->parameters[$key] = $value;
		return $this;
	}
	
	function setParameters(array $parameters) {
		$this->parameters = $parameters;
		
		return $this;
	}
	
	function removeParameter($key) {
		if (isset($this->parameters[$key]))
			unset($this->parameters[$key]);
	}
	
	/**
	 * Get all parameters
	 * @return array an array with all parameters.
	 */
	function getParameters() {
		return $this->parameters;
	}
	
	/**
	 * Get a parameter
	 * 
	 * @return string|NULL the parameter value
	 */
	function getParameter($key, $default=NULL) {
		if (isset($this->parameters[$key])) {
			return $this->parameters[$key];
		} else {
			return $default;
		}
	}

	/* -- End parameter handling -- */
	
	public function getActionScript()
	{
		$script = '';
        if (count($this->getParameters())) {
            $script .= 'setMultiPageState('.str_replace('"', "'", json_encode($this->getParameters())).');';
        } else {
            foreach($this->getParameters() as $key => $value) {
                $script .= 'setPageState(\''.$key.'\', \''.$value.'\');';
            }
        }
        
		return $script;
	}
}