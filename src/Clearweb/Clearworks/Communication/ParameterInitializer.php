<?php namespace Clearweb\Clearworks\Communication;

class ParameterInitializer extends ParameterChanger
{
	public function getActionScript()
	{
		$json_states = '{';
		$start = true;
		
		foreach($this->getParameters() as $key => $value) {
			if ( ! $start)
				$json_states .= ',';
            
            if (is_array($value)) {
                $value = implode(';', $value);
            }
			$json_states .= "'$key':'$value'";
			
			$start = false;
		}
		
		$json_states .= '}';
		
		$script = 'initPageState('.$json_states.');';
		
		return $script;
	}
}