<?php namespace Clearweb\Clearworks\Communication;

use Clearweb\Clearworks\Widget\Widget;
use Clearweb\Clearworks\Action\ScriptAction;

class ReloadCurrentWidget extends ScriptAction
{
	function getActionScript() {
		return 'updateCurrentWidget(context);';
	}
}