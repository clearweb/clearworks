<?php namespace Communication;

use \Clearweb\Clearworks\Contracts\IViewable;

abstract class ScriptAction implements IViewable
{
	abstract public function getActionScript();
	
	public function getScripts()
	{
		return array();
	}
	
	public function getStyles()
	{
		return array();
	}
	
	function getView()
	{
		$html = '<script type="text/javascript">'.PHP_EOL;
		$html .= $this->getActionScript().PHP_EOL;
		$html .= '</script>'.PHP_EOL;
		return $html;
	}
}