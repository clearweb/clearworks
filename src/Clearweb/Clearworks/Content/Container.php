<?php namespace Clearweb\Clearworks\Content;

use Clearweb\Clearworks\Contracts\IViewable;
use Clearweb\Clearworks\Contracts\IExecutable;


class Container implements IViewable {
	private $viewables = array();
	private $classes   = array();
	private $html_attr = array();
    private $id        = '';
	
	public function addViewable(IViewable $viewable, $name=null, $position=0) {
		$viewable = array(
						  'name'=>$name,
						  'viewable'=>$viewable
						  );
		
		if ($position > 0) {
			$this->viewables = array_merge(array_slice($this->viewables, 0, $position-1), array($viewable), array_slice($this->viewables, $position-1));
		} else {
			$this->viewables[] = $viewable;
		}
		return $this;
	}
	
	public function removeViewable($name) {
		$found_viewable = null;
			
		foreach($this->viewables as $i=>$viewable) {
			if ($viewable['name'] == $name) {
				unset($this->viewables[$i]);
			}
		}
	}
	
	public function hasViewable($name) {
		return ( ! is_null($this->getViewable($name)));
	}
	
	public function getViewable($name) {
		$found_viewable = null;
			
		foreach($this->viewables as $viewable) {
			if ($viewable['name'] == $name) {
				$found_viewable = $viewable['viewable'];
			}
		}
		
		return $found_viewable;
	}
    
    function getViewables() {
        return $this->viewables;
    }
    
    public function getID()
    {
        return $this->id;
    }
    
    public function setID($id)
    {
        $this->id = $id;
        return $this;
    }
    
	/**
	 * Clears all the registered viewables
	 */
	public function clearViewables()
	{
		$this->viewables = array();
		
		return $this;
	}
	
	public function addClass($class) {
		$this->classes[] = $class;
		return $this;
	}
	
	public function removeClass($class) {
		foreach($this->classes as $i => $currentClass) {
			if ($currentClass == $class) {
				unset($this->classes[$i]);
			}
		}
		
		return $this;
	}
    
	public function setClasses(array $classes) {
		$this->classes = $classes;
		return $this;
	}
	
	public function getClasses() {
		return $this->classes;
	}
	
	public function addHTMLAttribute($attr, $value) {
		$this->html_attr[$attr] = $value;
		return $this;
	}
	
	public function getHTMLAttributes() {
		return $this->html_attr;
	}
	

	
	/* --- IViewable implementation --- */
	
	public function getView() {
		$attributes = '';
		foreach($this->getHTMLAttributes() as $key=>$value) {
			//$attributes .= ' '.$key.'='.json_encode($value).'';
			$attributes .= ' '.$key.'="'.$value.'"';
		}
		
		$html = '<div '.$attributes.' class="'.implode(' ', $this->getClasses()).'">';
		$html .= array_reduce(
							  $this->viewables,
							  function($html, $viewable) {
                                  if ($viewable['viewable'] instanceof IExecutable) {
                                      $html .= $viewable['viewable']->__getView();
                                  } else {
                                      $html .= $viewable['viewable']->getView();
                                  }
                                  
								  return $html;
							  },
							  '');
		$html .= '</div>';
		return $html;
	}
	
	public function getScripts() {
		$scripts = array_reduce(
							 $this->viewables,
							 function($scripts, $viewable) {
								 $scripts = array_merge($scripts, $viewable['viewable']->getScripts());
								 return $scripts;
							 },
							 array());
		return $scripts;
	}	
	
	public function getStyles() {
		$styles = array_reduce(
							 $this->viewables,
							 function($styles, $viewable) {
								 $styles = array_merge($styles, $viewable['viewable']->getStyles());
								 return $styles;
							 },
							 array());
		return $styles;
	}

}