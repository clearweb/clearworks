<?php namespace Clearweb\Clearworks\Content;

use Clearweb\Clearworks\Contracts\IViewable;

class RawHtml implements IViewable
{
	private $html='';
	
	/**
	 * Set the HTML of the html viewable
	 * @param string $html the conten html
	 * @return $this
	 */
	function setHTML($html) {
		$this->html = $html;
		return $this;
	}
	
	/**
	 * Get the HTML of the html viewable
	 * @return string the html.
	 */
	function getHTML() {
		return $this->html;
	}
	
	function getView() {
		return $this->html;
	}
	
	function getScripts() {
		return array();
	}
	
	function getStyles() {
		return array();
	}
}