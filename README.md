# ClearWorks
This is an attempt to make a framework which enables users to create websites using
widget-based design. This framework features an easy way to implement inter-widget
communication and AJAX.

## Why this initiative
As a webdesign agency, we build a lot of websites. Our standard frameworks for these 
websites are WordPress and Prestashop. WordPress is actually build for blogs and 
the biggest percentage of the websites we build are informational websites.
As for Prestashop, it is as most other webshop software, very extensive. However
when the user want to have a custom widget/module it is not trivial to change the
standard behaviour without breaking/hacking into the core.

## Using PHP frameworks (mostly MVC)
Using a PHP framework for each standard informative website is not our favorite
way to work as it is custom work each time. Additionally, a lot of customers ask
for a CMS system. This is not trivial to build each time. Reusing code is not
as we like to see in most of those frameworks as someone should add the controller,
the model and the view in different directories. We long to a way to add a function,
mostly represented in a ui block (widget) by just copying the code of it to the 
framework and using it. 

## Mashup? Or just widget programming?
The style of the framework is inspired by existing mashup systems. However it is not
really meant to be a classic mashup system which is actually a shopping system where 
you can buy mashups and place them on your website. Actually you could implement this
using this framework, but its main purpose is for easily building AJAX websites and
reusing already developed widgets.